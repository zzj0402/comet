import textdistance

def cometSimilarity(str1,str2):
    if str1==str2:
        return 100
    hammingS=textdistance.hamming.similarity(str1,str2)/((len(str1)+len(str2))/2)
    mlipnsS=textdistance.mlipns.similarity(str1,str2)/((len(str1)+len(str2))/2)
    levenshteinS=textdistance.levenshtein.similarity(str1,str2)/((len(str1)+len(str2))/2)
    damerauLevenshteinS=textdistance.damerau_levenshtein.similarity(str1,str2)/((len(str1)+len(str2))/2)
    similarity=100*(hammingS*0.25+mlipnsS*0.25+levenshteinS*0.25+damerauLevenshteinS*0.25)
    return(similarity)