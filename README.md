# Comet

A composite string metric

## Prerequisite

Python 3
Pip

## Install

clone repository;
pip install -r requirements.text.

## Usage

### Terminal

python cli.py
input string 1
input string 2
Comet will print out similarity percentage rating.