import unittest
import textdistance
import comet

class TestComet(unittest.TestCase):
    def test_hamming(self):
        self.assertEqual(textdistance.hamming.similarity('test', 'text'), 3)
    def test_comet(self):
        self.assertEqual(comet.cometSimilarity('test', 'test'), 100)
        self.assertEqual(comet.cometSimilarity('', ''), 100)
        self.assertEqual(comet.cometSimilarity('test', ''), 0)
        self.assertEqual(comet.cometSimilarity('', 'test'), 0)

if __name__ == '__main__':
    unittest.main()